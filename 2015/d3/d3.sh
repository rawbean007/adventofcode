#!/bin/bash

X=1000
Y=1000
DB="db.txt"
rm -f "$DB"
echo "1000,1000" > "$DB"

cat in.txt | while IFS= read -r -n1 char; do
    case $char in
        '>')
            X=$((X+1))
            ;;
        '<')
            X=$((X-1))
            ;;
        'v')
            Y=$((Y-1))
            ;;
        '^')
            Y=$((Y+1))
            ;;
    esac
    if ! grep -q "$X,$Y" "$DB"; then
        echo "$X,$Y" >> "$DB"
    fi
done
wc -l "$DB"
