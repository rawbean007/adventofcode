#!/bin/zsh

function min {
    min_a=$1
    min_b=$2
    if [ $1 -lt $2 ]; then
        if [ $3 -lt $2 ]; then
            min_b=$3
        fi
    else
        if [ $3 -lt $1 ]; then
            min_a=$3
        fi
    fi
    echo $((min_a*min_b))
}

function rib {
    min_a=$1
    min_b=$2
    p=$(($1*$2*$3))
    if [ $1 -lt $2 ]; then
        if [ $3 -lt $2 ]; then
            min_b=$3
        fi
    else
        if [ $3 -lt $1 ]; then
            min_a=$3
        fi
    fi
    echo $((2*min_a+2*min_b+p))
}

export S=0
cat input.txt | while read line; do
    a=$(echo $line | cut -d\x -f1)
    b=$(echo $line | cut -d\x -f2)
    c=$(echo $line | cut -d\x -f3)
    m=$(min $a $b $c)
    s=$((2*a*b+2*a*c+2*b*c+m))
    echo $s
done | while read line; do
    S=$((S+line))
done
echo "1: $S"

export S2=0
cat input.txt | while read line; do
    a=$(echo $line | cut -d\x -f1)
    b=$(echo $line | cut -d\x -f2)
    c=$(echo $line | cut -d\x -f3)
    rib $a $b $c
done | while read line; do
    S2=$((S2+line))
done
echo "2: $S2"
