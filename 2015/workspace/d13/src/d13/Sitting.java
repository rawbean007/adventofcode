package d13;

import java.util.List;

public class Sitting {
	private String name;
	private Integer happiness;
	private List<String> remaining;

	public Sitting(String name, Integer happiness, List<String> remaining) {
		this.name = name;
		this.happiness = happiness;
		this.remaining = remaining;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getHappiness() {
		return happiness;
	}

	public void setHappiness(Integer happiness) {
		this.happiness = happiness;
	}

	public List<String> getRemaining() {
		return remaining;
	}

	public void setRemaining(List<String> remaining) {
		this.remaining = remaining;
	}
}