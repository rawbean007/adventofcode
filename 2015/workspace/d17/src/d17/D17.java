package d17;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class D17 {

	private static List<Integer> containers = new ArrayList<Integer>();

	private static Integer COMBINATIONS;

	public static void main(String[] args) {
		int nbCombinations = 0;
		BufferedReader b = null;
		try {
			File f = new File("input");
			b = new BufferedReader(new FileReader(f));
			String line = null;
			while ((line = b.readLine()) != null) {
				containers.add(Integer.valueOf(line));
			}
			b.close();
			COMBINATIONS = new Double(Math.pow(2, containers.size())).intValue();
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (int i = 0; i < COMBINATIONS; i++) {
			char[] s = String.format("%" + containers.size() + "s", Integer.toBinaryString(i)).replace(' ', '0')
					.toCharArray();
			int total = 0;
			for (int j = 0; j < s.length; j++) {
				if ('1' == s[j])
					total += containers.get(j);
			}
			if (total == 150)
				nbCombinations++;
		}
		System.out.println(nbCombinations);
	}
}
