package d14;

public class Reindeer {
	private String name;
	private Integer speed;
	private Integer sprintDuration;
	private Integer restDuration;

	public Reindeer(String name, Integer speed, Integer sprintDuration, Integer restDuration) {
		this.name = name;
		this.speed = speed;
		this.sprintDuration = sprintDuration;
		this.restDuration = restDuration;
	}

	@Override
	public String toString() {
		return name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSpeed() {
		return speed;
	}

	public void setSpeed(Integer speed) {
		this.speed = speed;
	}

	public Integer getSprintDuration() {
		return sprintDuration;
	}

	public void setSprintDuration(Integer sprintDuration) {
		this.sprintDuration = sprintDuration;
	}

	public Integer getRestDuration() {
		return restDuration;
	}

	public void setRestDuration(Integer restDuration) {
		this.restDuration = restDuration;
	}
}
