package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"strings"
)

func calc(filename string) (int, int) {
	allAnswers := aoc.ReadByBlocks(filename)
	result := 0
	result2 := 0
	for _, groupAnswers := range allAnswers {
		setOfGroupAnswers := aoc.NewStringSet()
		setOfCommonAnswers := aoc.NewStringSet()
		for i, answersStr := range groupAnswers {
			personAnswers := strings.Split(answersStr, "")
			if i == 0 {
				setOfCommonAnswers.AddAll(personAnswers)
			} else {
				setOfPersonAnswers := aoc.NewStringSet()
				setOfPersonAnswers.AddAll(personAnswers)
				setOfCommonAnswers = setOfCommonAnswers.Intersection(setOfPersonAnswers)
			}
			setOfGroupAnswers.AddAll(personAnswers)
		}
		result += setOfGroupAnswers.Size()
		result2 += setOfCommonAnswers.Size()
	}
	return result, result2
}

func main() {
	t1, t2 := calc("test.txt")
	aoc.CheckEquals(11, t1)
	aoc.CheckEquals(6, t2)
	r1, r2 := calc("input.txt")
	fmt.Printf("Result1: %d\n", r1)
	fmt.Printf("Result2: %d\n", r2)
}
