package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
)

const exp1 = 7
const exp2 = 336

func readInput(filename string) ([][]int, int, int) {
	lines := aoc.ReadInputLines(filename)
	height := len(lines)
	width := len(lines[0])
	result := make([][]int, height)
	for i := 0; i < height; i++ {
		result[i] = make([]int, width)
		for j := 0; j < width; j++ {
			if lines[i][j:j+1] == "#" {
				result[i][j] = 1
			}
		}
	}
	return result, height, width
}

func countTrees(forest [][]int, height, width, mx, my int) int {
	x, y := 0, 0
	trees := 0
	for y < height {
		trees += forest[y][x]
		x = (x + mx) % width
		y += my
	}
	return trees
}

func calc1(filename string) int {
	forest, height, width := readInput(filename)
	mx, my := 3, 1
	return countTrees(forest, height, width, mx, my)
}

func calc2(filename string) int {
	forest, height, width := readInput(filename)
	mxs := []int{1, 3, 5, 7, 1}
	mys := []int{1, 1, 1, 1, 2}
	result := 1
	for i, mx := range mxs {
		my := mys[i]
		result *= countTrees(forest, height, width, mx, my)
	}
	return result
}

func main() {
	aoc.CheckEqualsMsg(exp1, calc1("test.txt"), "test1")
	aoc.CheckEqualsMsg(exp2, calc2("test.txt"), "test2")
	r1 := calc1("input.txt")
	r2 := calc2("input.txt")
	fmt.Printf("Result 1: %d\n", r1)
	fmt.Printf("Result 2: %d\n", r2)
}
