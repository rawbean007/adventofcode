exp1 = 4
exp2 = 32


def cleanup_str(in_str):
    for del_word in ["bags", "bag", "."]:
        in_str = in_str.replace(del_word, "")
    nb_bags = None
    in_str = in_str.strip()
    if in_str[0].isdigit():
        number_split = in_str.split()
        nb_bags, in_str = int(number_split[0]), ' '.join(number_split[1:])
    return in_str.strip(), nb_bags


def read_input(filename):
    with open(filename, "r") as of:
        result = {}
        all_lines = list(map(str.strip, of.readlines()))
        for line in all_lines:
            rule_split = line.split("bags contain")
            parent, _ = cleanup_str(rule_split[0])
            children_raw = rule_split[1].split(",")
            children = {}
            for child_raw in children_raw:
                child, nb_bags = cleanup_str(child_raw)
                if child != "no other":
                    children.update({child: nb_bags})
            result.update({parent: children})
        return result


def browse_bags(result: set, candidates: set, data: dict) -> set:
    n_result = len(result)
    for candidate in [candidate for candidate in candidates
                      if any([r in data[candidate].keys() for r in result])]:
        candidates.remove(candidate)
        result.add(candidate)
    new_n_result = len(result)
    if n_result == new_n_result:
        return result
    return browse_bags(result, candidates, data)


def calc(filename):
    data = read_input(filename)
    result = browse_bags({"shiny gold"}, set(data.keys()), data)
    return len(result) - 1


def browse_bags_2(result: dict, to_add: dict, data: dict) -> dict:
    new_to_add = {}
    for color, nb in to_add.items():
        already_present = 0
        if color in result:
            already_present = result[color]
        result.update({color: nb + already_present})
        for c_color, c_nb in data[color].items():
            c_already_present = 0
            if c_color in new_to_add:
                c_already_present = new_to_add[c_color]
            new_to_add.update({c_color: nb * c_nb + c_already_present})
    if len(new_to_add) == 0:
        return result
    return browse_bags_2(result, new_to_add, data)


def calc2(filename):
    data = read_input(filename)
    result = browse_bags_2({}, {"shiny gold": 1}, data)
    r2 = sum(result.values()) - 1
    return r2


if __name__ == '__main__':
    assert calc("test.txt") == exp1
    assert calc2("test.txt") == exp2
    print(f"answer 1: {calc('input.txt')}")
    print(f"answer 2: {calc2('input.txt')}")
