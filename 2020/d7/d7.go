package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"strconv"
	"strings"
)

const exp1 = 4
const exp2 = 32

var delWords = []string{"bags", "bag", "."}

func cleanupStr(inStr string) (string, int) {
	for _, word := range delWords {
		inStr = strings.ReplaceAll(inStr, word, "")
	}
	inStr = strings.Trim(inStr, " ")
	n := 0
	for _, ascii := range inStr[0:1] {
		if ascii >= 48 && ascii <= 57 {
			numberSplit := strings.Split(inStr, " ")
			n, _ = strconv.Atoi(numberSplit[0])
			inStr = strings.Join(numberSplit[1:], " ")
		}
	}
	return strings.Trim(inStr, " "), n
}

func readInput(filename string) *map[string]map[string]int {
	result := make(map[string]map[string]int)
	allLines := aoc.ReadInputLines(filename)
	for _, line := range allLines {
		ruleSplit := strings.Split(line, "bags contain")
		parent, _ := cleanupStr(ruleSplit[0])
		childrenRaw := strings.Split(ruleSplit[1], ",")
		children := make(map[string]int)
		for _, childRaw := range childrenRaw {
			child, nbBags := cleanupStr(childRaw)
			if child != "no other" {
				children[child] = nbBags
			}
		}
		result[parent] = children
	}
	return &result
}

func browseBags(result, candidates *aoc.StringSet, data *map[string]map[string]int) *aoc.StringSet {
	nbResults := result.Size()
	toTransfer := aoc.NewStringSet()
	for candidate := range candidates.Iterate() {
		goodCandidate := false
		for child := range (*data)[candidate] {
			if result.Contains(child) {
				goodCandidate = true
				break
			}
		}
		if goodCandidate {
			toTransfer.Add(candidate)
		}
	}
	for candidate := range toTransfer.Iterate() {
		candidates.Remove(candidate)
		result.Add(candidate)
	}
	newNbResult := result.Size()
	if newNbResult == nbResults {
		return result
	}
	return browseBags(result, candidates, data)
}

func calc(data *map[string]map[string]int) int {
	shinyGoldSet := aoc.NewStringSet()
	shinyGoldSet.Add("shiny gold")
	allColors := aoc.NewStringSet()
	for color := range *data {
		allColors.Add(color)
	}
	result := browseBags(shinyGoldSet, allColors, data)
	return result.Size() - 1
}

func browseBags2(result, toAdd map[string]int, data *map[string]map[string]int) map[string]int {
	newToAdd := make(map[string]int)
	for color, nb := range toAdd {
		alreadyPresent := result[color]
		result[color] = nb + alreadyPresent
		for cColor, cNb := range (*data)[color] {
			cAlreadyPresent := newToAdd[cColor]
			newToAdd[cColor] = nb*cNb + cAlreadyPresent
		}
	}
	if len(newToAdd) == 0 {
		return result
	}
	return browseBags2(result, newToAdd, data)
}

func calc2(data *map[string]map[string]int) int {
	shinyGold := make(map[string]int)
	shinyGold["shiny gold"] = 1
	allBags := browseBags2(make(map[string]int), shinyGold, data)
	result := 0
	for _, nbBags := range allBags {
		result += nbBags
	}
	return result - 1
}

func main() {
	testData := readInput("test.txt")
	t1 := calc(testData)
	t2 := calc2(testData)
	aoc.CheckEquals(exp1, t1)
	aoc.CheckEquals(exp2, t2)
	data := readInput("input.txt")
	r1 := calc(data)
	r2 := calc2(data)
	fmt.Printf("Result1: %d\n", r1)
	fmt.Printf("Result2: %d\n", r2)
}
