package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"regexp"
	"sort"
	"strings"
)

const (
	exp1 = 5
	exp2 = "mxmxvkd,sqjhc,fvjkl"
)

var r = regexp.MustCompile(`([a-z ]*) \(contains ([a-z, ]*)\)`)

func parseRecipe(recipe string, allIngredients []string, allAllergens *aoc.StringSet, allergenToIngredientsMap map[string]*aoc.StringSet) []string {
	matches := r.FindStringSubmatch(recipe)
	ingredientsList := strings.Split(matches[1], " ")
	ingredients := aoc.NewStringSet()
	ingredients.AddAll(ingredientsList)
	allIngredients = append(allIngredients, ingredientsList...)
	allergens := strings.Split(matches[2], ", ")
	allAllergens.AddAll(allergens)
	for _, allergen := range allergens {
		_, exists := allergenToIngredientsMap[allergen]
		if !exists {
			allergenToIngredientsMap[allergen] = ingredients
		} else {
			allergenToIngredientsMap[allergen] = allergenToIngredientsMap[allergen].Intersection(ingredients)
		}
	}
	return allIngredients
}

func readInput(filename string) ([]string, []string, *map[string]*aoc.StringSet, map[string]string) {
	recipes := aoc.ReadInputLines(filename)
	allergenToIngredientsMap := make(map[string]*aoc.StringSet)
	allIngredients := make([]string, 0)
	allAllergens := aoc.NewStringSet()
	for _, recipe := range recipes {
		allIngredients = parseRecipe(recipe, allIngredients, allAllergens, allergenToIngredientsMap)
	}
	sortedAllergens := allAllergens.ToList()
	sort.Strings(sortedAllergens)
	ingredientToAllergenMap := cleanupMapping(&allergenToIngredientsMap)
	return allIngredients, sortedAllergens, &allergenToIngredientsMap, ingredientToAllergenMap
}

func removeUniqueCandidateFromOtherMappings(allergenToIngredientsMap *map[string]*aoc.StringSet) {
	for allergen, candidates := range *allergenToIngredientsMap {
		if candidates.Size() == 1 {
			candidate := candidates.ToList()[0]
			for otherAllergen, otherCandidates := range *allergenToIngredientsMap {
				if otherAllergen != allergen && otherCandidates.Contains(candidate) {
					otherCandidates.Remove(candidate)
				}
			}
		}
	}
}

func cleanupMapping(allergenToIngredientsMap *map[string]*aoc.StringSet) map[string]string {
	goOn := true
	for goOn {
		removeUniqueCandidateFromOtherMappings(allergenToIngredientsMap)
		goOn = false
		for _, candidates := range *allergenToIngredientsMap {
			if candidates.Size() != 1 {
				goOn = true
			}
		}
	}
	ingredientToAllergenMap := make(map[string]string)
	for allergen, ingredientMap := range *allergenToIngredientsMap {
		ingredient := ingredientMap.ToList()[0]
		ingredientToAllergenMap[ingredient] = allergen
	}
	return ingredientToAllergenMap
}

func countIngredientsWithNoAllergens(allIngredients []string, ingredientToAllergenMap map[string]string) int {
	result := 0
	for _, ingredient := range allIngredients {
		_, exists := ingredientToAllergenMap[ingredient]
		if !exists {
			result++
		}
	}
	return result
}

func orderedListOfIngredientsWithAllergens(sortedAllergens []string, allergenToIngredientsMap *map[string]*aoc.StringSet) string {
	sb := &strings.Builder{}
	for i, allergen := range sortedAllergens {
		if i > 0 {
			sb.WriteString(",")
		}
		sb.WriteString((*allergenToIngredientsMap)[allergen].ToList()[0])
	}
	return sb.String()
}

func calc(filename string) (int, string) {
	allIngredients, sortedAllergens, allergenToIngredientsMap, ingredientToAllergenMap := readInput(filename)
	part1 := countIngredientsWithNoAllergens(allIngredients, ingredientToAllergenMap)
	part2 := orderedListOfIngredientsWithAllergens(sortedAllergens, allergenToIngredientsMap)
	return part1, part2
}

func main() {
	t1, t2 := calc("test.txt")
	aoc.CheckEqualsMsg(exp1, t1, "test1")
	if exp2 != t2 {
		aoc.Log(fmt.Sprintf("test2: expected %%v, got %%v"), exp2, t2)
	}
	r1, r2 := calc("input.txt")
	fmt.Printf("Result1: %d\n", r1)
	fmt.Printf("Result2: %s\n", r2)
}
