package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
)

type coord struct {
	y int
	x int
}

var shift = map[string]*coord{
	"e":  {0, 2},
	"se": {1, 1},
	"ne": {-1, 1},
	"w":  {0, -2},
	"sw": {1, -1},
	"nw": {-1, -1},
}

func (c *coord) moveCoords(c2 *coord) {
	c.y += c2.y
	c.x += c2.x
}

func (c *coord) addCoords(c2 *coord) coord {
	return coord{c.y + c2.y, c.x + c2.x}
}

func readTile(tileStr string) coord {
	curPos := 0
	result := coord{0, 0}
	for curPos < len(tileStr) {
		cardinal := tileStr[curPos : curPos+1]
		if cardinal == "n" || cardinal == "s" {
			curPos++
			cardinal += tileStr[curPos : curPos+1]
		}
		result.moveCoords(shift[cardinal])
		curPos++
	}
	return result
}

func readInput(filename string) map[coord]int {
	tileStrs := aoc.ReadInputLines(filename)
	tiles := make(map[coord]int)
	for _, tileStr := range tileStrs {
		tileCoord := readTile(tileStr)
		tiles[tileCoord] = 1 - tiles[tileCoord]
	}
	return tiles
}

func countBlackTiles(tiles map[coord]int) int {
	result := 0
	for _, side := range tiles {
		result += side
	}
	return result
}

func countBlackNeighbours(tiles map[coord]int, tile coord) int {
	result := 0
	for _, movement := range shift {
		result += tiles[tile.addCoords(movement)]
	}
	return result
}

func calc(filename string) int {
	tiles := readInput(filename)
	return countBlackTiles(tiles)
}

func calc2(filename string) int {
	tiles := readInput(filename)
	xMin, xMax, yMin, yMax := 0, 0, 0, 0
	for location := range tiles {
		xMin = aoc.Min(xMin, location.x)
		xMax = aoc.Max(xMax, location.x)
		yMin = aoc.Min(yMin, location.y)
		yMax = aoc.Max(yMax, location.y)
	}
	// WTF?
	parity := yMin * xMin % 2
	for day := 1; day <= 100; day++ {
		xMin, xMax, yMin, yMax = xMin-2, xMax+2, yMin-1, yMax+1
		tilesToChange := make([]coord, 0)
		for y := yMin; y <= yMax; y++ {
			for x := xMin - (y+parity)%2; x <= xMax+(y+parity)%2; x += 2 {
				curTile := coord{y, x}
				n := countBlackNeighbours(tiles, curTile)
				tileColor := tiles[curTile]
				if (tileColor == 1 && (n == 0 || n > 2)) || (tileColor == 0 && n == 2) {
					tilesToChange = append(tilesToChange, curTile)
				}
			}
		}
		for _, tileToChange := range tilesToChange {
			tiles[tileToChange] = 1 - tiles[tileToChange]
		}
	}
	return countBlackTiles(tiles)
}

func main() {
	aoc.CheckEqualsMsg(10, calc("test.txt"), "test1")
	aoc.CheckEqualsMsg(2208, calc2("test.txt"), "test2")
	fmt.Printf("Result 1: %d\n", calc("input.txt"))
	fmt.Printf("Result 2: %d\n", calc2("input.txt"))
}
