package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"strconv"
	"strings"
)

const exp1 = 2
const exp2 = 1

func extractInfo(l string) (int, int, string, string) {
	parts := strings.Split(l, " ")
	nbs := strings.Split(parts[0], "-")
	minO, _ := strconv.Atoi(nbs[0])
	maxO, _ := strconv.Atoi(nbs[1])
	char := parts[1]
	char = strings.ReplaceAll(char, ":", "")
	pwd := parts[2]
	return minO, maxO, char, pwd
}

func calc1(filename string) int {
	lines := aoc.ReadInputLines(filename)
	count := 0
	for _, l := range lines {
		minO, maxO, char, pwd := extractInfo(l)
		nOcc := strings.Count(pwd, char)
		if nOcc >= minO && nOcc <= maxO {
			count++
		}
	}
	return count
}

func calc2(filename string) int {
	lines := aoc.ReadInputLines(filename)
	count := 0
	for _, l := range lines {
		minO, maxO, char, pwd := extractInfo(l)
		if (pwd[minO-1:minO] == char) != (pwd[maxO-1:maxO] == char) {
			count++
		}
	}
	return count
}

func main() {
	aoc.CheckEqualsMsg(exp1, calc1("test.txt"), "test1")
	aoc.CheckEqualsMsg(exp2, calc2("test.txt"), "test2")
	r1 := calc1("input.txt")
	r2 := calc2("input.txt")
	fmt.Printf("Result 1: %d\n", r1)
	fmt.Printf("Result 2: %d\n", r2)
}
