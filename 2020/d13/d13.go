package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"strconv"
	"strings"
)

const exp1 = 295
const exp21 = 1068781
const exp22 = 3417
const exp23 = 754018
const exp24 = 779210
const exp25 = 1261476
const exp26 = 1202161486

func calc(filename string) int {
	data := aoc.ReadInputLines(filename)
	now, _ := strconv.Atoi(data[0])
	minWait := now
	busMinWait := 0
	for _, busId := range strings.Split(data[1], ",") {
		period, err := strconv.Atoi(busId)
		if err == nil {
			wait := period - now%period
			if wait == period {
				wait = 0
			}
			if wait < minWait {
				minWait = wait
				busMinWait = period
			}
		}
	}
	return busMinWait * minWait
}

func invert(quotient, period int) int {
	initialPeriod := period
	x0 := 0
	x1 := 1
	if period == 1 {
		return 0
	}
	for quotient > 1 {
		q := quotient / period
		t := period
		period = quotient % period
		quotient = t
		t = x0
		x0 = x1 - q*x0
		x1 = t
	}
	if x1 < 0 {
		x1 += initialPeriod
	}
	return x1
}

func chineseRemainderTheorem(busPeriods, waits []int) int {
	// https://en.wikipedia.org/wiki/Chinese_remainder_theorem
	productPeriods := 1
	for _, n := range busPeriods {
		productPeriods *= n
	}
	result := 0
	for i, period := range busPeriods {
		quotient := productPeriods / period
		result += waits[i] * invert(quotient, period) * quotient
	}
	return result % productPeriods
}

func calc2(filename string) int {
	data := aoc.ReadInputLines(filename)
	schedule := strings.Split(data[1], ",")
	busPeriods := make([]int, 0)
	waits := make([]int, 0)
	for i, busId := range schedule {
		if busId != "x" {
			period, _ := strconv.Atoi(busId)
			busPeriods = append(busPeriods, period)
			waits = append(waits, period-i)
		}
	}
	result := chineseRemainderTheorem(busPeriods, waits)
	return result
}

func main() {
	aoc.CheckEqualsMsg(exp1, calc("test.txt"), "test1")
	aoc.CheckEqualsMsg(exp21, calc2("test.txt"), "test21")
	aoc.CheckEqualsMsg(exp22, calc2("test2.txt"), "test22")
	aoc.CheckEqualsMsg(exp23, calc2("test3.txt"), "test23")
	aoc.CheckEqualsMsg(exp24, calc2("test4.txt"), "test24")
	aoc.CheckEqualsMsg(exp25, calc2("test5.txt"), "test25")
	aoc.CheckEqualsMsg(exp26, calc2("test6.txt"), "test26")
	r1 := calc("input.txt")
	r2 := calc2("input.txt")
	fmt.Printf("Result1: %d\n", r1)
	fmt.Printf("Result2: %d\n", r2)
}
