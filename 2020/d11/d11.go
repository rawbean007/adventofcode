package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
)

const exp1 = 37
const exp2 = 26

func readInput(filename string) [][]int {
	floorPlan := aoc.ReadInputLines(filename)
	result := make([][]int, len(floorPlan))
	for rowNb, seats := range floorPlan {
		result[rowNb] = make([]int, len(seats))
		for colNb, seat := range seats {
			result[rowNb][colNb] = int((seat - 46) / 30)
		}
	}
	return result
}

func nAround(floorPlan [][]int, y, x int) int {
	nb := 0
	fromRow := aoc.Max(y-1, 0)
	toRow := aoc.Min(y+2, len(floorPlan))
	fromCol := aoc.Max(x-1, 0)
	toCol := aoc.Min(x+2, len(floorPlan[y]))
	for i := fromRow; i < toRow; i++ {
		for j := fromCol; j < toCol; j++ {
			if (i != y || j != x) && floorPlan[i][j] == 2 {
				nb++
			}
		}
	}
	return nb
}

func nAround2(floorPlan [][]int, y, x int) int {
	nb := 0
	nRows := len(floorPlan)
	rowLen := len(floorPlan[y])
	for i := y - 1; i >= 0; i-- {
		if floorPlan[i][x] != 0 {
			nb += floorPlan[i][x] - 1
			break
		}
	}
	for i := y + 1; i < nRows; i++ {
		if floorPlan[i][x] != 0 {
			nb+= floorPlan[i][x] - 1
			break
		}
	}
	for j := x - 1; j >= 0; j-- {
		if floorPlan[y][j] != 0 {
			nb+= floorPlan[y][j] - 1
			break
		}
	}
	for j := x + 1; j < rowLen; j++ {
		if floorPlan[y][j] != 0 {
			nb+= floorPlan[y][j] - 1
			break
		}
	}
	for k := 1; k < len(floorPlan); k++ {
		if y-k >= 0 && x-k >= 0 {
			if floorPlan[y-k][x-k] != 0 {
				nb+= floorPlan[y-k][x-k] - 1
				break
			}
		}
	}
	for k := 1; k < len(floorPlan); k++ {
		if y-k >= 0 && x+k < rowLen {
			if floorPlan[y-k][x+k] != 0 {
				nb+= floorPlan[y-k][x+k] - 1
				break
			}
		}
	}
	for k := 1; k < len(floorPlan); k++ {
		if y+k < nRows && x-k >= 0 {
			if floorPlan[y+k][x-k] != 0 {
				nb+= floorPlan[y+k][x-k] - 1
				break
			}
		}
	}
	for k := 1; k < len(floorPlan); k++ {
		if y+k < nRows && x+k < rowLen {
			if floorPlan[y+k][x+k] != 0 {
				nb+= floorPlan[y+k][x+k] - 1
				break
			}
		}
	}
	return nb
}

func calc(floorPlan [][]int) int {
	stateChanged := false
	nextState := make([][]int, len(floorPlan))
	nOccupied := 0
	for rowNb := range nextState {
		currentRowSeats := floorPlan[rowNb]
		nextState[rowNb] = make([]int, len(currentRowSeats))
		for colNb := range currentRowSeats {
			currentSeatState := currentRowSeats[colNb]
			if currentSeatState != 0 {
				nextState[rowNb][colNb] = currentSeatState
				neighbours := nAround(floorPlan, rowNb, colNb)
				if currentSeatState == 1 && neighbours == 0 {
					stateChanged = true
					nextState[rowNb][colNb] = 2
				} else if currentSeatState == 2 && neighbours > 3 {
					stateChanged = true
					nextState[rowNb][colNb] = 1
				}
				if currentSeatState == 2 {
					nOccupied++
				}
			}
		}
	}
	if stateChanged {
		return calc(nextState)
	}
	return nOccupied
}

func calc2(floorPlan [][]int) int {
	stateChanged := false
	nextState := make([][]int, len(floorPlan))
	nOccupied := 0
	for rowNb := range nextState {
		currentRowSeats := floorPlan[rowNb]
		nextState[rowNb] = make([]int, len(currentRowSeats))
		for colNb := range currentRowSeats {
			currentSeatState := currentRowSeats[colNb]
			if currentSeatState != 0 {
				nextState[rowNb][colNb] = currentSeatState
				neighbours := nAround2(floorPlan, rowNb, colNb)
				if currentSeatState == 1 && neighbours == 0 {
					stateChanged = true
					nextState[rowNb][colNb] = 2
				} else if currentSeatState == 2 && neighbours > 4 {
					stateChanged = true
					nextState[rowNb][colNb] = 1
				}
				if currentSeatState == 2 {
					nOccupied++
				}
			}
		}
	}
	if stateChanged {
		return calc2(nextState)
	}
	return nOccupied
}

func main() {
	testPlan := readInput("test.txt")
	t1 := calc(testPlan)
	t2 := calc2(testPlan)
	aoc.CheckEqualsMsg(exp1, t1, "test1")
	aoc.CheckEqualsMsg(exp2, t2, "test2")
	floorPlan := readInput("input.txt")
	r1 := calc(floorPlan)
	r2 := calc2(floorPlan)
	fmt.Printf("result 1: %d\n", r1)
	fmt.Printf("result 2: %d\n", r2)
}
