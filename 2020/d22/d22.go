package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"strconv"
	"strings"
)

func readInput(filename string) (*[]int, *[]int) {
	decks := aoc.ReadByBlocks(filename)
	cards1 := parseDeck(decks[0])
	cards2 := parseDeck(decks[1])
	return cards1, cards2
}

func parseDeck(deck []string) *[]int {
	cards := make([]int, len(deck)-1)
	for i, cardStr := range deck[1:] {
		cards[i], _ = strconv.Atoi(cardStr)
	}
	return &cards
}

func playRound(cardsP1, cardsP2 *[]int) bool {
	c1 := (*cardsP1)[0]
	c2 := (*cardsP2)[0]
	if c1 < c2 {
		*cardsP1 = (*cardsP1)[1:]
		*cardsP2 = append((*cardsP2)[1:], c2, c1)
	} else {
		*cardsP1 = append((*cardsP1)[1:], c1, c2)
		*cardsP2 = (*cardsP2)[1:]
	}
	return len(*cardsP1)*len(*cardsP2) != 0
}

func calc(filename string) int {
	cards1, cards2 := readInput(filename)
	for playRound(cards1, cards2) {
	}
	result := 0
	cards := *cards1
	if len(cards) == 0 {
		cards = *cards2
	}
	for idx, card := range cards {
		result += (len(cards) - idx) * card
	}
	return result
}

func deckToLayout(cards1, cards2 []int) string {
	result := strings.Builder{}
	for _, card := range cards1 {
		result.WriteString(strconv.Itoa(card))
	}
	result.WriteString("|")
	for _, card := range cards2 {
		result.WriteString(strconv.Itoa(card))
	}
	return result.String()
}

func playGame2(cards1 []int, cards2 []int) ([]int, []int, int) {
	layouts := aoc.NewStringSet()
	layouts.Add(deckToLayout(cards1, cards2))
	for len(cards1)*len(cards2) != 0 {
		cards1, cards2 = playRound2(cards1, cards2)
		layout := deckToLayout(cards1, cards2)
		if layouts.Contains(layout) {
			return cards1, cards2, 1
		}
		layouts.Add(layout)
	}
	if len(cards1) == 0 {
		return cards1, cards2, 2
	}
	return cards1, cards2, 1
}

func playRound2(cards1, cards2 []int) ([]int, []int) {
	c1 := cards1[0]
	c2 := cards2[0]
	cards1 = cards1[1:]
	cards2 = cards2[1:]
	var roundWinner int
	if c1 <= len(cards1) && c2 <= len(cards2) {
		newDeck1 := make([]int, len(cards1[:c1]))
		newDeck2 := make([]int, len(cards2[:c2]))
		copy(newDeck1, cards1[:c1])
		copy(newDeck2, cards2[:c2])
		_, _, roundWinner = playGame2(newDeck1, newDeck2)
	} else {
		roundWinner = 1
		if c1 < c2 {
			roundWinner = 2
		}
	}
	if roundWinner == 1 {
		cards1 = append(cards1, c1, c2)
	} else {
		cards2 = append(cards2, c2, c1)
	}
	return cards1, cards2
}

func calc2(filename string) int {
	cards1Ptr, cards2Ptr := readInput(filename)
	cards1, cards2, winner := playGame2(*cards1Ptr, *cards2Ptr)
	cards := cards1
	if winner == 2 {
		cards = cards2
	}
	result := 0
	for idx, card := range cards {
		result += (len(cards) - idx) * card
	}
	return result
}

func main() {
	aoc.CheckEqualsMsg(306, calc("test.txt"), "test1")
	aoc.CheckEqualsMsg(291, calc2("test.txt"), "test2")
	fmt.Printf("result 1: %d\n", calc("input.txt"))
	fmt.Printf("result 2: %d\n", calc2("input.txt"))
}
