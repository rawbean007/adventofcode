package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"regexp"
	"strconv"
	"strings"
)

const exp1 = 2
const exp2 = 12

type ruleDesc struct {
	numbers  []int
	letter   string
	ruleStr  string
	category string
	ruleId   int
}

var regexpRules = map[string]*regexp.Regexp{
	"directRule":     regexp.MustCompile(`^([0-9]+): "([a-z])"$`),
	"simpleRule":     regexp.MustCompile(`^([0-9]+): ([0-9]+) ([0-9]+)$`),
	"threeNumbers":   regexp.MustCompile(`^([0-9]+): ([0-9]+) ([0-9]+) ([0-9]+)$`),
	"complexRule":    regexp.MustCompile(`^([0-9]+): ([0-9]+) ([0-9]+) \| ([0-9]+) ([0-9]+)$`),
	"identityRule":   regexp.MustCompile(`^([0-9]+): ([0-9]+)$`),
	"eitherRule":     regexp.MustCompile(`^([0-9]+): ([0-9]+) \| ([0-9]+)$`),
	"unbalancedRule": regexp.MustCompile(`^([0-9]+): ([0-9]+) \| ([0-9]+) ([0-9]+)$`),
}

var makeRuleRegex = map[string]func(*ruleDesc, map[int]string) string{
	"directRule": func(rule *ruleDesc, _ map[int]string) string {
		return rule.letter
	},
	"simpleRule": func(rule *ruleDesc, m map[int]string) string {
		return fmt.Sprintf("%s%s", m[rule.numbers[0]], m[rule.numbers[1]])
	},
	"threeNumbers": func(rule *ruleDesc, m map[int]string) string {
		return fmt.Sprintf("%s%s%s", m[rule.numbers[0]], m[rule.numbers[1]], m[rule.numbers[2]])
	},
	"complexRule": func(rule *ruleDesc, m map[int]string) string {
		return fmt.Sprintf(`(?:%s%s|%s%s)`, m[rule.numbers[0]], m[rule.numbers[1]], m[rule.numbers[2]], m[rule.numbers[3]])
	},
	"identityRule": func(rule *ruleDesc, m map[int]string) string {
		return m[rule.numbers[0]]
	},
	"eitherRule": func(rule *ruleDesc, m map[int]string) string {
		return fmt.Sprintf(`(?:%s|%s)`, m[rule.numbers[0]], m[rule.numbers[1]])
	},
	"unbalancedRule": func(rule *ruleDesc, m map[int]string) string {
		return fmt.Sprintf(`%s|%s%s`, m[rule.numbers[0]], m[rule.numbers[1]], m[rule.numbers[2]])
	},
}

var makeRecursiveRule = map[string]func(*ruleDesc, map[int]string) string{
	"unbalancedRule": func(rule *ruleDesc, m map[int]string) string {
		return fmt.Sprintf(`(?:%s)*%s`, m[rule.numbers[0]], m[rule.numbers[1]])
	},
	"threeNumbers": func(rule *ruleDesc, m map[int]string) string {
		return fmt.Sprintf(`%s{n}%s{n}`, m[rule.numbers[0]], m[rule.numbers[2]])
	},
}

func findDerivableRule(dependencies map[int]*aoc.IntSet, parsedRules map[int]string,
	nonParsedRules map[int]*ruleDesc) (*ruleDesc, bool) {
	for ruleId := range parsedRules {
		for newRuleId := range dependencies[ruleId].Iterate() {
			if _, alreadyDone := parsedRules[newRuleId]; !alreadyDone {
				skipToNext := false
				flagRecurse := false
				for _, n := range nonParsedRules[newRuleId].numbers {
					_, known := parsedRules[n]
					if !known {
						if n == newRuleId {
							flagRecurse = true
						} else {
							skipToNext = true
							break
						}
					}
				}
				if !skipToNext {
					return nonParsedRules[newRuleId], flagRecurse
				}
			}
		}
	}
	return nil, false
}

func readRules(rulesStr []string) map[int]string {
	parsedRules := make(map[int]string)
	dependencies := make(map[int]*aoc.IntSet)
	nonParsedRules := make(map[int]*ruleDesc)
	for _, ruleStr := range rulesStr {
		for cat, reg := range regexpRules {
			if reg.MatchString(ruleStr) {
				var numbers []int
				var letter string
				matches := reg.FindStringSubmatch(ruleStr)
				ruleId, _ := strconv.Atoi(matches[1])
				if _, rootDepExists := dependencies[ruleId]; !rootDepExists {
					dependencies[ruleId] = aoc.NewIntSet()
				}
				if cat != "directRule" {
					numbers = make([]int, len(matches)-2)
					for i := 0; i < len(matches)-2; i++ {
						numbers[i], _ = strconv.Atoi(matches[i+2])
						if _, depExists := dependencies[numbers[i]]; !depExists {
							dependencies[numbers[i]] = aoc.NewIntSet()
						}
						dependencies[numbers[i]].Add(ruleId)
					}
				} else {
					letter = matches[2]
					parsedRules[ruleId] = letter
				}
				nonParsedRules[ruleId] = &ruleDesc{
					numbers:  numbers,
					letter:   letter,
					ruleStr:  ruleStr,
					category: cat,
					ruleId:   ruleId,
				}
			}
		}
	}
	for len(parsedRules) < len(rulesStr) {
		if rule, recurse := findDerivableRule(dependencies, parsedRules, nonParsedRules); !recurse {
			parsedRules[rule.ruleId] = makeRuleRegex[rule.category](rule, parsedRules)
		} else {
			parsedRules[rule.ruleId] = makeRecursiveRule[rule.category](rule, parsedRules)
		}
	}
	return parsedRules
}

func checkAllMessages(rule0Regex string, messages []string) int {
	result := 0
	check := regexp.MustCompile(rule0Regex)
	for _, m := range messages {
		if check.MatchString(m) {
			result++
		}
	}
	return result
}

func calc(filename string) int {
	blocks := aoc.ReadByBlocks(filename)
	messages := blocks[1]
	rule0 := fmt.Sprintf("^%s$", readRules(blocks[0])[0])
	if strings.Contains(rule0, "{n}") {
		result := 0
		for i := 1; i < 6; i++ {
			ithRegex := strings.ReplaceAll(rule0, "{n}", fmt.Sprintf("{%d}", i))
			result += checkAllMessages(ithRegex, messages)
		}
		return result
	} else {
		return checkAllMessages(rule0, messages)
	}
}

func main() {
	t1 := calc("test.txt")
	t2 := calc("test2.txt")
	aoc.CheckEqualsMsg(exp1, t1, "test1")
	aoc.CheckEqualsMsg(exp2, t2, "test2")
	r1 := calc("input.txt")
	r2 := calc("input2.txt")
	fmt.Printf("result 1: %d\n", r1)
	fmt.Printf("result 2: %d\n", r2)
}
