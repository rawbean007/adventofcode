| Whom | Where | What |
|:------------:|:-----------------------------------------------------------------:|:-------:|
| Alfred| [GitLab/Alfred456654](https://www.gitlab.com/Alfred456654/adventofcode) | Go & Python 3 |
| Benjamin | [GitHub/baudren](https://github.com/baudren/advent-of-code) | Nim |
| Charles | [GitLab/picheryc](https://gitlab.com/picheryc/adventofcode2020/) | Python 3 |
| Dave | [GitLab/dchiron88](https://gitlab.com/dchiron88/aoc2020) | Python 3 |
| Florian | [GitLab/FloMibu](https://gitlab.com/FloMibu/adventofcode) | Python 2 (lol) |
| Guillaume | [GitHub/GuillaumeSmaha](https://github.com/GuillaumeSmaha/advent-of-code) | Go & Python 3 |
| Ikspe | [GitLab/ikspe](https://gitlab.com/ikspe/advent-of-code-2021) | Python 3 |
| Marlox | [GitHub/marlox-ouda](https://github.com/marlox-ouda/aoc) | Python 3, Go, C |
| R0nd0ud0u | [GitHub.com/r0nd0ud0u](https://github.com/r0nd0ud0u/aoc2020) | C++ |
| Pierre | [GitLab/pelyot](https://gitlab.com/pelyot/adventofcode) | Rust |
| PtiLuky | [GitHub/PtiLuky](https://github.com/PtiLuky/aoc) | Rust |
| RobinOffZeWood | [GitHub/robinoffzewood](https://github.com/robinoffzewood/aoc-2020) | Rust |
| William | [GitHub/willi-am-public](https://github.com/willi-am-publi/aoc2020) | Python 3 |
