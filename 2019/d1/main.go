package main

import (
	"bufio"
	"os"
	"strconv"
)

func fuel(mass int) int {
	result := mass/3 - 2
	if result > 0 {
		return result
	}
	return 0
}

func fuelRec(mass, acc int) int {
	fuelMass := fuel(mass)
	if fuelMass != 0 {
		return fuelRec(fuelMass, acc+fuelMass)
	}
	return acc
}

func main() {
	input, _ := os.Open("input")

	scanner := bufio.NewScanner(input)

	contents := make([]int, 100)
	result := 0
	result2 := 0
	i := 0
	for scanner.Scan() {
		contents[i], _ = strconv.Atoi(scanner.Text())
		i++
	}
	for _, a := range contents {
		result += fuel(a)
		result2 += fuelRec(a, 0)
	}

	_ = result
	_ = result2
}
