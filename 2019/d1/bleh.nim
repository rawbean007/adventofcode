import strutils
import sequtils
import math
import sugar

let contents = readFile("input").splitLines.map(parseInt)

proc fuel(mass: int): int =
    result = max(mass div 3 - 2, 0)

proc fuel_rec(mass: int, acc: int = 0): int =
    var fuel_mass = fuel(mass)
    if fuel_mass != 0:
        result = fuel_rec(fuel_mass, acc+fuel_mass)
    else:
        result = acc


discard contents.map(fuel).sum
discard contents.map(mass => fuel_rec(mass)).sum
