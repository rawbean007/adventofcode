from hashlib import md5
input = 'ugkcyxxp'
finished = False
i = 0
answer = [''] * 8
while not finished:
    i += 1
    h = md5()
    h.update(input + str(i))
    if h.hexdigest().startswith('00000'):
        n = ord(h.hexdigest()[5])
        if n > 47 and n < 56 and answer[n - 48] == '':
            answer[n - 48] = h.hexdigest()[6]
            finished = answer.count('') == 0
print(''.join(answer))
