f = open('input', 'r')
l = f.readlines()[0].split(', ')
x,y = 0,0
turn_dir = {'R': -1, 'L': 1}
incr_x = {0: 0, 1: -1, 2: 0, 3: 1}
incr_y = {0: 1, 1: 0, 2: -1, 3: 0}
visited_places = []
cur_dir = 0
found = 0
for i in l:
    if found:
        break
    cur_dir = (cur_dir + turn_dir[i[0]]) % 4
    nb_blocks = int(i[1:].strip())
    incr = incr_x[cur_dir] + incr_y[cur_dir]
    if incr_y[cur_dir] == 0:
        for j in range(0, incr * nb_blocks, incr):
            x = x + incr
            if [x, y] in visited_places:
                print('%d,%d already visited, it was the %dth block visited' % (x, y, visited_places.index([x, y])))
                found = 1
                break
            visited_places.append([x, y])
    else:
        for j in range(0, incr * nb_blocks, incr):
            y = y + incr
            if [x, y] in visited_places:
                print('%d,%d already visited, it was the %dth block visited' % (x, y, visited_places.index([x, y])))
                found = 1
                break
            visited_places.append([x, y])
print('answer: %d' % (abs(x) + abs(y)))
