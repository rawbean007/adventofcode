offset = 1362
departure = (1, 1)
max_moves = 50
def formula(x, y):
    return bin(x*x + 3*x + 2*x*y + y + y*y + offset)[2:].count('1') % 2
def get_neighbours(visited, node):
    x = node[0]
    y = node[1]
    answer = [ ]
    for i in range(x - 1, x + 2, 2):
        if i >= 0 and formula(i, y) == 0 and not (i, y) in visited:
            answer.append((i, y))
    for j in range(y - 1, y + 2, 2):
        if j >= 0 and formula(x, j) == 0 and not (x, j) in visited:
            answer.append((x, j))
    return answer
if __name__ == '__main__':
    queue = [ departure ]
    visited = [ departure ]
    tree = { str(departure): 0 }
    while queue:
        node = queue.pop(0)
        for adj in get_neighbours(visited, node):
            path_len = tree[str(node)] + 1
            if path_len <= 50:
                visited.append(adj)
                tree.update({str(adj): path_len})
                queue.append(adj)
    print(len(visited))
