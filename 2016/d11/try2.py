from threading import Thread
from time import sleep

stop_thread = False
dirs = {-1: 'below', 1: 'above'}
queue = [ ]

def show(building):
    print('')
    print('  E0011')
    print('   GMGM')
    for y in range(4):
        line = '%d ' % (3 - y)
        for x in building:
            if x == 3 - y:
                line += 'X'
            else:
                line += ' '
        print(line)
    print('')

def list_moves(m_chips, gens, other_m_chips, other_gens, direct):
    direct_str = dirs[direct]
    moves = []
    for m in m_chips:
        #print('considering moving chip %d %s' % (m, direct_str))
        move_this_chip = (m not in other_gens and len(other_gens) > 1) or m in other_gens or len(other_gens) == 0
        if move_this_chip:
            #print('  OK to move chip %d %s' % (m, direct_str))
            moves.append((direct, [2*m+2]))
        if m in gens:
            #print('considering to move gen+chip %d %s' % (m, direct_str))
            move_this_pair = True
            for mm in other_m_chips:
                #print('  checking with chip %d' % mm)
                if mm not in other_gens:
                    #print('    KO: conflict between chip %d and gen %d' % (mm, m))
                    move_this_pair = False
            if move_this_pair:
                #print('  OK to move gen+chip %d %s' % (m, direct_str))
                moves.append((direct, [2*m+1, 2*m+2]))
        for mm in m_chips:
            if m > mm:
                move_these_chips = True
                #print('considering moving chips %d and %d %s' % (m, mm, direct_str))
                for gg in other_gens:
                    #print('  checking with gen %d' % gg)
                    if (gg != m and m not in other_gens) or (gg != mm and mm not in other_gens):
                        #print('    KO: conflict between chips %d, %d and gen %d' % (m, mm, gg))
                        move_these_chips = False
                        break
                if move_these_chips:
                    moves.append((direct, [2*m+2, 2*mm+2]))
    for g in gens:
        #print('considering to move gen %d %s' % (g, direct_str))
        move_gen_ok = True
        if g in m_chips:
            if len(gens) > 1:
                move_gen_ok = False
        if move_gen_ok:
            for m in other_m_chips:
                #print('  checking with chip %d' % m)
                if m != g and m not in other_gens:
                    #print('    KO: conflict between gen %d and chip %d' % (g, m))
                    move_gen_ok = False
                    break
        if move_gen_ok:
            #print('  OK to move gen %d %s' % (g, direct_str))
            moves.append((direct, [2*g+1]))
        for gg in gens:
            if g > gg:
                #print('considering to move gens %d and %d' % (g, gg))
                move_gens_pair = True
                for mm in other_m_chips:
                    #print('  checking with chip %d' % mm)
                    if mm not in other_gens and mm != gg and mm != g:
                        #print('    KO: conflict between gens %d, %d and chip %d' % (g, gg, mm))
                        move_gens_pair = False
                if move_gens_pair:
                    moves.append((direct, [2*g+1, 2*gg+1]))
    return moves

def valid_moves(building):
    #show(building)
    moves = []
    elev_pos = building[0]
    items = building[1:]
    if elev_pos < 0 or elev_pos > 3:
        return moves
    m_chips = [ i for i, x in enumerate(items[1::2]) if x == elev_pos ]
    gens = [ i for i, x in enumerate(items[::2]) if x == elev_pos ]

    if elev_pos > 0:
        below_m_chips = [ i for i, x in enumerate(items[1::2]) if x == elev_pos - 1 ]
        below_gens = [ i for i, x in enumerate(items[::2]) if x == elev_pos - 1 ]
        for m in list_moves(m_chips, gens, below_m_chips, below_gens, -1):
            moves.append(m)

    if elev_pos < 3:
        above_m_chips = [ i for i, x in enumerate(items[1::2]) if x == elev_pos + 1 ]
        above_gens = [ i for i, x in enumerate(items[::2]) if x == elev_pos + 1 ]
        for m in list_moves(m_chips, gens, above_m_chips, above_gens, 1):
            moves.append(m)

    #print('--> OK moves: %s' % moves)
    return moves

def make_move(building, move):
    for obj in move[1]:
        building[obj] += move[0]
    building[0] += move[0]
    return building

def get_hash(building):
    return ''.join(list(map(str, building)))

def show_queue_length():
    while not stop_thread:
        sleep(5)
        print(len(queue))

if __name__ == '__main__':
    thread = Thread(target = show_queue_length, args=())
    thread.start()
    init_building = [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3]
    #init_building = [3, 3, 3, 3, 3]
    #init_building = [0, 1, 0, 2, 0]
    init_hash = get_hash(init_building)
    target = '00012121212'
    #target = '01020'
    #target = '33333'
    building = [ x for x in init_building ]
    queue.append(building)
    visited = [ init_hash ]
    tree = { }
    hash_values = {init_hash: 0}
    while queue:
        building = queue.pop(0)
        #show(building)
        h = get_hash(building)
        hash_val = hash_values[h]
        if h == target:
            stop_thread = True
            fallback = h
            cpt = 0
            while fallback != init_hash:
                this_b = list(map(int, fallback))
                show(this_b)
                fallback = tree[fallback]
                cpt += 1
            this_b = list(map(int, fallback))
            show(this_b)
            print(cpt)
            thread.join()
            break
        #print('------------------------')
        #print('Checking hash %s (value %d)' % (h, hash_val))
        if h in tree:
            previous_building = tree[h]
            #print('predecessor:')
            #show(list(map(int, previous_building)))
        moves = valid_moves(building)
        #print('%d moves possible: %s' % (len(moves), moves))
        for m in moves:
            new_building = make_move([x for x in building], m)
            new_hash = get_hash(new_building)
            if new_hash not in visited:
                #print('New hash: %s (value: %d)' % (new_hash, hash_val + 1))
                visited.append(new_hash)
                queue.append(new_building)
                tree.update({new_hash: h})
                hash_values.update({new_hash: hash_val + 1})
            #else:
                #print('Already seen: %s (value: %d; now would be %d)' % (new_hash, hash_values[new_hash], hash_val + 1))
