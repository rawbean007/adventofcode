import sys

def analyze(in_file):
    answer = ''
    with open(in_file, 'r') as f:
        l = f.readline().strip()
        """
        modes :
        0   normal
        1   read seq_len
        2   read nb_rep
        3   read seq_to_rep
        """
        mode = 0
        seq_len = 0
        nb_rep = 0
        seq_to_rep = ''
        for c in l:
            if mode == 0:
                if c == '(':
                    mode = 1
                else:
                    answer += c
            elif mode == 1:
                if c == 'x':
                    mode = 2
                else:
                    seq_len = 10 * seq_len + int(c)
            elif mode == 2:
                if c == ')':
                    mode = 3
                else:
                    nb_rep = 10 * nb_rep + int(c)
            elif mode == 3:
                if seq_len > 0:
                    seq_len -= 1
                    seq_to_rep += c
                else:
                    for j in range(nb_rep):
                        answer += seq_to_rep
                    seq_len = 0
                    nb_rep = 0
                    seq_to_rep = ''
                    if c == '(':
                        mode = 1
                    else:
                        answer += c
                        mode = 0
    if mode == 3:
        for j in range(nb_rep):
            answer += seq_to_rep
        seq_len = 0
        nb_rep = 0
        seq_to_rep = ''

    answer = answer.strip()
    print(len(answer))

if __name__ == '__main__':
    for j in sys.argv[1:]:
        analyze(j)
