#!/usr/bin/python

def format_input(filename):
    with open(filename, 'r') as f:
        in_str = list(map(str.strip, f.readlines()))
        return {int(x[0]): int(x[1]) for x in list(map(lambda y: y.split(': '), in_str))}

def severity(fw, delay):
    caught = [x * fw[x] for x in fw.keys() if (x + delay) % ((fw[x] - 1) * 2) == 0]
    return max(len(caught), sum(caught))

def calc(filename):
    fw = format_input(filename)
    a1 = severity(fw, 0)
    delay = 1
    while severity(fw, delay) != 0:
        delay += 1
    return (a1, delay)

if __name__ == '__main__':
    c = calc('test')
    # print('test: %d, %d' % c)
    assert c == (24, 10)
    print('answers: %d, %d' % calc('input'))
