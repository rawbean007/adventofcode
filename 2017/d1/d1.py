#!/usr/bin/python

test1 = {'1122': 3, '1111': 4, '1234': 0, '91212129': 9}
test2 = {'1212': 6, '1221': 0, '123425': 4, '123123': 12, '12131415': 4}

def calc1(in_str):
    return sum([int(in_str[i]) for i in range(len(in_str)) if in_str[i-1] == in_str[i]])

def calc2(in_str):
    l = int(len(in_str) / 2)
    return 2 * sum([int(in_str[i]) for i in range(l) if in_str[i] == in_str[(i + l)]])

if __name__ == '__main__':
    # print("Input String | Output | Expected")
    for k in test1.keys():
        # print("%12s | %6d | %d" % (k, calc1(k), test1[k]))
        assert calc1(k) == test1[k]
    # print("\nInput String | Output | Expected")
    for k in test2.keys():
        # print("%12s | %6d | %d" % (k, calc2(k), test2[k]))
        assert calc2(k) == test2[k]
    with open('input', 'r') as f:
        in_str = f.read().strip()
        a1 = calc1(in_str)
        print("answer 1: %d" % a1)
        a2 = calc2(in_str)
        print("answer 2: %d" % a2)
