#!/usr/bin/python

import numpy as np
from itertools import chain

START = '.#./..#/###'
CONV = {'.': 0, '#': 1}
ACONV = {0: '.', 1: '#'}

def conv(s):
    return np.array([[CONV[y] for y in chain(x)] for x in s.split('/')])

def disp(a, oneline=True):
    return ['\n','/'][oneline].join([[' ',''][oneline].join([ACONV[x] for x in y]) for y in a])

def modif(m, screen):
    if m == 0:
        return screen[::-1]
    elif m == 1:
        return np.array([x[::-1] for x in screen])
    elif m > 1 and m < 5:
        return np.rot90(screen, k = m - 1)
    elif m > 4 and m < 8:
        return np.rot90(screen[::-1], k = m - 4)
    elif m > 7 and m < 11:
        return np.rot90(np.array([x[::-1] for x in screen]), k = m - 7)
    else:
        raise Exception('%d | %s' % (m, screen))

def calc(filename, n):
    with open(filename, 'r') as f:
        rules = {x.split(' => ')[0]:x.split(' => ')[1] for x in f.read().strip().split('\n')}
        rules = {str(conv(k)): conv(v) for k, v in rules.items()}
        screen = conv(START)
        for it in range(n):
            L = len(screen)
            p = [2, 3][L % 2]
            l = int(L/p)
            squares = [screen[p*i:p*i+p,p*j:p*j+p] for i in range(l) for j in range(l)]
            for i in range(len(squares)):
                s = squares[i]
                ns = s
                m = 0
                while not str(ns) in rules:
                    ns = modif(m, s)
                    m += 1
                squares[i] = ns
            new_squares = [rules[str(x)] for x in squares]
            screen = np.concatenate([np.concatenate([new_squares[l*y+x] for x in range(l)], axis = 1) for y in range(l)])
        return np.sum(screen)

if __name__ == '__main__':
    assert calc('test', 2) == 12
    print('Answer 1: %d' % calc('input', 5))
    print('Answer 2: %d' % calc('input', 18))
