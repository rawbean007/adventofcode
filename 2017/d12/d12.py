#!/usr/bin/python

def prepare(filename):
    with open(filename, 'r') as f:
        return {int(x.split(' ')[0]): set(map(int, ''.join(x.split(' ')[2:]).split(','))) for x in
                f.read().strip().split('\n')}

def reachable(v, villages, visited, to_visit):
    visited.add(v)
    new_to_visit = villages[v].difference(visited)
    to_visit = to_visit.union(new_to_visit)
    if len(to_visit) > 0:
        return visited.union(reachable(list(to_visit)[0], villages, visited, set(list(to_visit)[1:])))
    else:
        return visited

def calc(filename):
    villages = prepare(filename)
    group0 = reachable(0, villages, set([]), set([]))
    n_groups = 1
    all_villages = set(villages.keys()).difference(group0)
    while len(all_villages) > 0:
        new_group = reachable(list(all_villages)[0], villages, set([]), set([]))
        n_groups += 1
        all_villages = all_villages.difference(new_group)
    return (len(group0), n_groups)

if __name__ == '__main__':
    assert calc('test') == (6, 2)
    print('answers: %d, %d' % calc('input'))

