#!/usr/bin/python

from collections import Counter
import numpy as np


def parse_boxid(in_str):
    s = set(Counter([b for b in in_str]).values())
    return [1 if 2 in s else 0, 1 if 3 in s else 0]


def calc(filename):
    with open(filename, 'r') as of:
        values = list(map(str.strip, of.readlines()))
        nb = {2: 0, 3: 0}
        result = np.sum([parse_boxid(boxid) for boxid in values], axis=0)
        return result[0] * result[1]


def similarity(a, b):
    return len([i for i in range(len(a)) if a[i] != b[i]])


def make_common(a, b):
    return ''.join([a[i] for i in range(len(a)) if a[i] == b[i]])


def calc2(filename):
    with open(filename, 'r') as of:
        values = list(map(str.strip, of.readlines()))
        for iw1 in range(len(values)):
            w1 = values[iw1]
            for iw2 in range(iw1, len(values)):
                w2 = values[iw2]
                if similarity(w1, w2) == 1:
                    return make_common(w1, w2)


if __name__ == '__main__':
    assert calc('test') == 12
    print(f"result 1: {calc('input')}")
    assert calc2('test2') == 'fgij'
    print(f"result 2: {calc2('input')}")

