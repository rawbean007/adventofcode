#!/usr/bin/python

import itertools


def calc():
    with open('input', 'r') as of:
        return sum([int(nb) for nb in list(map(str.strip, of.readlines()))])


def calc2(filename):
    with open(filename, 'r') as of:
        nbs = list(map(int, list(map(str.strip, of.readlines()))))
        freqs_reached = set([0])
        freq = 0
        while True:
            for nb in nbs:
                freq += nb
                if freq in freqs_reached:
                    return freq
                freqs_reached.add(freq)


def calc2_alt(filename):
    with open(filename, 'r') as of:
        nbs = list(map(int, of.readlines()))
        freqs_reached = set([0])
        freq = 0
        for n in itertools.cycle(nbs):
            freq += n
            if freq in freqs_reached:
                return freq
            freqs_reached.add(freq)


def test2():
    expected_results = [2, 0, 10, 5, 14]
    for t in range(5):
        assert calc2(f'test{t}') == expected_results[t]


if __name__ == '__main__':
    print(f'result 1: {calc()}')
    test2()
    print(f'result 2: {calc2("input")}')
