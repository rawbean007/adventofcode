#!/usr/bin/python


def addr(regs, a, b, c):
    regs[c] = regs[a] + regs[b]
    return regs


def addi(regs, a, b, c):
    regs[c] = regs[a] + b
    return regs


def mulr(regs, a, b, c):
    regs[c] = regs[a] * regs[b]
    return regs


def muli(regs, a, b, c):
    regs[c] = regs[a] * b
    return regs


def banr(regs, a, b, c):
    regs[c] = regs[a]&regs[b]
    return regs


def bani(regs, a, b, c):
    regs[c] = regs[a]&b
    return regs


def borr(regs, a, b, c):
    regs[c] = regs[a]|regs[b]
    return regs


def bori(regs, a, b, c):
    regs[c] = regs[a]|b
    return regs


def setr(regs, a, b, c):
    regs[c] = regs[a]
    return regs


def seti(regs, a, b, c):
    regs[c] = a
    return regs


def gtir(regs, a, b, c):
    regs[c] = 1 if a > regs[b] else 0
    return regs


def gtri(regs, a, b, c):
    regs[c] = 1 if regs[a] > b else 0
    return regs


def gtrr(regs, a, b, c):
    regs[c] = 1 if regs[a] > regs[b] else 0
    return regs


def eqir(regs, a, b, c):
    regs[c] = 1 if a == regs[b] else 0
    return regs


def eqri(regs, a, b, c):
    regs[c] = 1 if regs[a] == b else 0
    return regs


def eqrr(regs, a, b, c):
    regs[c] = 1 if regs[a] == regs[b] else 0
    return regs


OPERATIONS = [addr, addi, mulr, muli, banr, bani, borr, bori, setr, seti, gtir, gtri, gtrr, eqir, eqri, eqrr]


def disambiguate(opcodes):
    while len([v for v in opcodes.values() if type(v) is set]) > 0:
        for opcode in [k for k, v in opcodes.items() if type(v) is set and len(v) == 1]:
            opcodes[opcode] = opcodes[opcode].pop()
            for other_op_code in [k for k, v in opcodes.items() if k != opcode and type(v) is set]:
                if opcodes[opcode] in opcodes[other_op_code]:
                    opcodes[other_op_code].remove(opcodes[opcode])
    return opcodes


def parse_history(history):
    blocks = history.split('\n\n')
    opcodes = {}
    answer_1 = 0
    for b in blocks:
        before, operation, after = b.split('\n')
        regs_b = eval(before.replace('Before: ', ''))
        [opcode, a, b, c] = [int(t) for t in operation.split(' ')]
        regs_a = eval(after.replace('After: ', ''))
        possible_ops = set([op for op in OPERATIONS if op(regs_b.copy(), a, b, c) == regs_a])
        if len(possible_ops) >= 3:
            answer_1 += 1
        if opcode not in opcodes:
            opcodes.update({opcode: possible_ops})
        else:
            opcodes.update({opcode: possible_ops.intersection(opcodes[opcode])})
    opcodes = disambiguate(opcodes)
    return answer_1, opcodes


def parse_instructions(instructions, opcodes):
    regs = [0, 0, 0, 0]
    lines = [list(map(int, l.split(' '))) for l in instructions.split('\n') if len(l) > 0]
    for l in lines:
        regs = opcodes[l[0]](regs, l[1], l[2], l[3])
    return regs[0]


def read_input(filename):
    with open(filename, 'r') as of:
        history, instructions = of.read().split('\n\n\n\n')
        answer_1, history = parse_history(history)
        answer_2 = parse_instructions(instructions, history)
        return answer_1, answer_2


if __name__ == '__main__':
    a1, a2 = read_input('input')
    print(f"Answer 1: {a1}")
    print(f"Answer 2: {a2}")
