#!/usr/bin/python

import re
import numpy as np


def calc():
    grid = np.ndarray((1000, 1000))
    with open('input', 'r') as of:
        squares_str = list(map(str.strip, of.readlines()))
        squares = [tuple(map(int, s.split())) for s in [re.sub(r'[@#:]', '', re.sub(r'[,x]', ' ', x)) for x in squares_str]]
        for s in squares:
            grid[s[2]:s[2]+s[4], s[1]:s[1]+s[3]] += 1
        result1 = np.sum(grid > 1)
        for s in squares:
            if np.sum(grid[s[2]:s[2]+s[4], s[1]:s[1]+s[3]]) == s[3] * s[4]:
                return result1, s[0]


if __name__ == '__main__':
    print(calc())
