#!/usr/bin/python

from collections import deque
import itertools


def read_input(filename):
    with open(filename, 'r') as of:
        l = of.readline().split()
        return int(l[0]), int(l[6])


def play(n_players, m_max):
    marble_idx = 0 # value of last marble picked
    scores = {i: 0 for i in range(n_players)}
    circle = deque([0])
    for p in itertools.cycle(range(n_players)):
        marble_idx += 1
        if marble_idx % 23 != 0:
            circle.rotate(-1)
            circle.append(marble_idx)
        else:
            scores[p] += marble_idx
            circle.rotate(8)
            gained = circle.popleft()
            scores[p] += gained
            circle.rotate(-1)
        if marble_idx >= m_max:
            return max(scores.values())


def calc(filename):
    n_players, m_max = read_input(filename)
    return play(n_players, m_max)

def calc2(filename):
    n_players, m_max = read_input(filename)
    return play(n_players, m_max * 100)


if __name__ == '__main__':
    expected_results = [32, 8317, 146373, 2764, 54718, 37305]
    for i in range(6):
        assert calc(f'test{i}') == expected_results[i]
    print(calc('input'))
    print(calc2('input'))
