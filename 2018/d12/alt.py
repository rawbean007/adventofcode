#!/usr/bin/python
### !!! ###                            ### !!! ###
# It doesn't actually work but it was fun, so... #
### !!! ###                            ### !!! ###

import re
import math

INIT = re.compile(r'^initial state: ([01]+)$')
RULE = re.compile(r'^([01]{5}) => ([01])$')


def read_input(filename):
    with open(filename, 'r') as of:
        all_lines = list(map(lambda s: s.replace('#', '1').replace('.', '0'), of.readlines()))
        init_state = int(INIT.match(all_lines[0]).group(1), 2)
        rules = {int(k, 2): int(v) for k, v in list(map(lambda s: RULE.match(s).groups(), all_lines[2:]))}
        return init_state, rules


def step(state, rules, offset):
    if state & 1:
        state *= 16
        offset += 4
    elif state & 2:
        state *= 8
        offset += 3
    elif state & 4:
        state *= 4
        offset += 2
    state_len = int(math.log2(state))
    return sum([rules[(state // (2 ** i)) & 31] * (2 ** i) for i in range(state_len)]), offset


def calc(filename, nb):
    state, rules = read_input(filename)
    print(bin(state)[2:])
    print(rules)
    print()
    print(f'0 {sum([k for k,v in enumerate(bin(state)[2:]) if v == "1"])}')
    print(bin(state)[2:])
    print()
    offset = 0
    for i in range(nb):
        state, offset = step(state, rules, offset)
        print(f'{i+1} {sum([k for k,v in enumerate(bin(state)[2:]) if v == "1"])}')
        print(bin(state)[2:])
        print(f'offset: {offset}')
        print()
    return sum([(k-offset) for k,v in enumerate(bin(state)[2:]) if v == "1"])


if __name__ == '__main__':
    print(calc('input', 6))
