#!/usr/bin/python

import re
from datetime import datetime


TIME_FMT = '%Y-%m-%d %H:%M'

def calc(filename):
    with open(filename, 'r') as of:
        cur_guard = -1
        cur_start_sleep = -1
        naps = {}
        total_sleep_time = {}
        for l in sorted(of.readlines()):
            items = re.sub("[\[\]]", "", l.strip()).split()
            ts = datetime.strptime(' '.join([items[0], items[1]]), TIME_FMT)
            if items[2] == 'Guard':
                cur_guard = int(items[3].replace('#', ''))
                cur_start_sleep = -1
            elif items[2] == 'falls' and cur_start_sleep == -1:
                cur_start_sleep = ts
            elif items[2] == 'wakes' and cur_start_sleep != -1:
                delta = ts - cur_start_sleep
                minute_start_sleep = cur_start_sleep.minute
                if cur_guard not in total_sleep_time:
                    total_sleep_time.update({cur_guard: 0})
                total_sleep_time[cur_guard] += int(delta.seconds / 60)
                for t in range(minute_start_sleep, minute_start_sleep + int(delta.seconds / 60)):
                    if (cur_guard, t % 60) in naps:
                        naps[(cur_guard, t % 60)] += 1
                    else:
                        naps.update({(cur_guard, t): 1})
                cur_start_sleep = -1
        guard_most_asleep = max(total_sleep_time, key = total_sleep_time.get)
        laziest_guard_nap_minutes = {n: t for n, t in naps.items() if n[0] == guard_most_asleep}
        result2 = max(naps, key=naps.get)
        result = max(laziest_guard_nap_minutes, key=laziest_guard_nap_minutes.get)
        return result[0] * result[1], result2[0] * result2[1]


if __name__ == '__main__':
    assert calc('test') == (240, 4455)
    print(calc('input'))
